FROM node:13.14.0-alpine
EXPOSE 3000

WORKDIR /app
ADD package.json /app/
RUN npm install

ADD . /app
CMD npm start