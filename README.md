# TimeOff.Management Gorilla Challenge
This repository contents a *TimeOff.Management* application fork from: https://github.com/timeoff-management/timeoff-management-application to implement a scheme CI/CD.
 
## Technology ecosystem implementation CI/CD
 
![pipeline-flow](docs/images/00-implementation-architecture.jpg)
 
## Folders and Files distribution
 
The most relevant files or folders are:
 
~~~
├── Jenkinsfile       #Pipeline implementation tasks
├── infraestructure   #Infrastructure
│   ├── tools         #Implementation Jenkins tool
│   ├── vm            #Create VM
│   └── ansible       #Provisioning VM
├── sonar.properties  #Properties sonar scan
└── Dockerfile        #Container image
~~~



